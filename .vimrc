set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Syntastic'
Plugin 'The-Nerd-tree'
Plugin 'javascript.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ctrlp.vim'
Plugin 'DoxygenToolkit.vim'
let g:DoxygenToolkit_briefTag_pre="@Description "
let g:DoxygenToolkit_authorName="Albert R. Cowie"

" All plugins above this line
call vundle#end()           " required
filetype plugin indent on   " required



set number


set tabstop=4
set shiftwidth=4
set noexpandtab

inoremap {<cr> {<cr>}<c-o><s-o>
inoremap [<cr> [<cr>]<c-o><s-o>
inoremap (<cr> (<cr>)<c-o><s-o>
let g:ycm_global_ycm_extra_conf = "~/.ycm_extra_conf.py"

"**Template starts here**
autocmd bufnewfile *.cpp,*.h,*.c so ~/Templates/headercpp.txt
autocmd bufnewfile makef* silent! execute '0r  ~/Templates/makeFile.txt'
autocmd bufnewfile *.cpp,*.h,*.c exe "1," . 10 . "g/File Name :.*/s//File Name : " .expand("%")
autocmd bufnewfile *.cpp,*.h,*.c  exe "1," . 10 . "g/Date :.*/s//Creation Date : " .strftime("%d-%m-%Y")
autocmd Bufwritepre,filewritepre *.cpp,*.h,*.c  exe "1," . 10 . "g/Last Modified :.*/s/Last Modified :.*/Last Modified : " .strftime("%c")
autocmd bufwritepost,filewritepost *.cpp,*.h,*.c  execute "normal `a"
autocmd bufnewfile *.cpp,*.h,*.c  exe "1," . 11 . "g/Created By :.*/s//Created By : " "A.R. Cowie"
let fileName = expand("%:t:r")
autocmd bufnewfile *.h exe  "1," . 13 . "g,\*/,s,\*/,\*/\r\r#ifndef ".fileName."_h\r#define " .fileName."_h\r\r\r\r\r#endif"
autocmd bufnewfile *.cpp exe  "1," . 13 . "g,\*/,s,\*/,\*/\r\r#include <iostream>\rusing namespace std;\r\r\r\rint main(int argc, char *argv[]){\r\r\r\r\r}"
autocmd bufnewfile *Mpi.cpp exe  "1," . 13 . "g,\*/,s,\*/,\*/\r\r#include <iostream>\r#include <mpi.h>\rusing namespace std;\r\r\r\rint main(int argc, char *argv[]){\r\r\r\r\r}"

"**Abbreviations start here**
iab bashb #!/bin/bash
iab awkb #!/bin/awk
iab mpiinit  
\<CR>int rank, size, ierr;
\<CR>
\<CR>
\<CR>ierr=MPI_Init(&argc, &argv);
\<CR>
\<CR>ierr=MPI_Comm_rank(MPI_COMM_WORLD, &rank);
\<CR>
\<CR>ierr=MPI_Comm_size(MPI_COMM_WORLD, &size);
\<CR>
\<CR>
\<CR>
\<CR>
\<CR>MPI_Finalize();

iab mpireduce 
\<CR>ierr=MPI_Reduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm);

iab mpibcast 
\<CR>ierr=MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm);

iab mpisend 
\<CR>ierr=MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_COM_WORLD);

iab mpirec 
\<CR>ierr=MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_COMM-WORLD, MPI_Status *status);

iab mpifileopen 
\<CR>ierr=MPI_File_open(MPI_COMM_WORLD, char *filename, MPI_MODE_CREATE<Bar>MPI_MODE_RDWR<Bar>MPI_MODE_RDONLY<Bar>MPI_MPI_MODE_APPEND<Bar>MPI_MODE_WRONLY, MPI_INFO_NULL, MPI_FILE *mpi_fh);

"Mappings start here
:imap <c-f> <ESC>:Dox<CR>
:imap <c-a> <ESC>:r ~/Templates/alienSigTemp.txt<CR>i
:imap <c-c> <ESC>yy<CR>i
:imap <c-x> <ESC>dd<CR>i
:imap <c-p> <ESC>p<CR>i
